$(function(){
  $.escapeHTML = function(val) {
    return $("<div/>").text(val).html();
  };
  chrome.tabs.getSelected(null, function(tab) {
   var siteTitle = tab.title;
   var siteUrl   = tab.url;
   var content;
   siteTitle = $.escapeHTML(siteTitle) + '.html';
   content = '<!DOCTYPE html><html><head><meta http-equiv="refresh" content="1; URL='+$.escapeHTML(siteUrl)+'"></head><body></body></html>'
   var blob = new Blob([content],{type:'text/plain'});
   $('#DownloadBtn').attr('href',URL.createObjectURL(blob));
   $('#DownloadBtn').attr('target','_blank');
   $('#DownloadBtn').attr('download',siteTitle);
 });
});